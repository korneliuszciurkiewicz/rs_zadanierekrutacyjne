<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Feature;

class FeaturesController extends Controller
{



    public function save(Request $request)
    {   


        $validated = $request->validate([
            'name' => 'required',
            'product_id' => 'required'

        ]);

        if($validated){


            $feature = new Feature();
            $feature->name = $request->input('name');
            $feature->product_id = $request->input('product_id');

            $feature->save();
        }
        return back()->withInput();


    }


    public function delete($id)
    {   
        $feature = Feature::where('id', $id)->first();
        $feature->delete();

        return back()->withInput();
    }




}
