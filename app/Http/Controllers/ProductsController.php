<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProductsController extends Controller
{
    public function index()
    {   

        $products = Product::all();
        $data = ['products'=> $products];
        return view('products', $data);
    }

    public function save(Request $request)
    {   


        $validated = $request->validate([
            'name' => 'required'
        ]);

        if($validated){


            $product = new Product();
            $product->name = $request->input('name');
            $product->description = $request->input('description');
            $product->price = $request->input('price');
            $product->pricing_period = $request->input('pricing_period');
            $product->promoted = $request->input('promoted');
            $product->discounted = $request->input('discounted');
            $product->save();
        }
        return back()->withInput();


    }


    public function update($id, Request $request)
    {   


        $validated = $request->validate([
            'name' => 'required'
        ]);

        if($validated){


            $product = Product::where('id', $id)->first();
            $product->name = $request->input('name');
            $product->description = $request->input('description');
            $product->price = $request->input('price');
            $product->pricing_period = $request->input('pricing_period');
            $product->promoted = $request->input('promoted');
            $product->discounted = $request->input('discounted');
            $product->save();
        }
        return back()->withInput();


    }


    public function delete($id)
    {   
        $product = Product::where('id', $id)->first();
        $product->features()->delete();
        $product->delete();

        return back()->withInput();
    }



}
