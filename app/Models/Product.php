<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Feature;

class Product extends Model
{
    use HasFactory;


    public function discounted($discountValue = 15){

        $this->price = $this->price-(($this->price * $discountValue)/100); 
        
    }

    public function getPriceAttribute($price)
    {
        return round($price, 2); 
    }

    public function features(){
        return $this->hasMany(Feature::class);
    }
}
