<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
<link rel="stylesheet" href="{{ asset('css/app.css') }}">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


<div class="navbar">
    <div class="container">
        <div class="navbar__logo">
            <img src="{{ asset('img/logo_big.svg') }}" alt="">
        </div>
    </div>
</div>

<div class="banner">
    <div class="container">
        <div class="banner__content">
            <div class="banner__title">
                Products
            </div>
            <div class="banner__actions">
                <a href="#" class="btn btn-outline-primary" data-toggle="modal" data-target="#addFeature">Add feature</a>
                <a href="#" class="btn btn-outline-primary" data-toggle="modal" data-target="#addPackage">Add package</a>
            </div>
        </div>
    </div>
</div>

<div class="section">
    <div class="container">
        <div class="row">
            @foreach ($products as $product)
            <div class="col-4">
                <div class="package">
                    <div class="package__actions-top">
                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#editPackage_{{$product->id}}">Edit package</a>
                        <a href="{{route('productDelete', ['id'=>$product->id])}}" class="btn btn-primary" onclick="return confirm('Are you sure?')">Delete package</a>
                    </div>
                    <div class="package__title">
                        {{ $product->name }}
                    </div>
                    <div class="package__price">
                        {{ $product->price }}
                    </div>
                    <div class="package__desc">
                        {{ $product->description }}
                    </div>
                    <div class="package__features">
                        <ul>
                            @foreach ($product->features as $feature)
                            <li>
                                <div class="package__feature">
                                    <strong>{{ $feature->name}}</strong> 
                                </div>
                                <div class="package__features-actions">
                                    <a href="{{route('featureDelete', ['id'=>$feature->id])}}" onclick="return confirm('Are you sure?')">
                                        <i class="zmdi zmdi-delete" ></i>
                                    </a>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="package__actions">
                        <a href="#" class="btn btn-primary">Get it now</a>
                    </div>
                </div>
            </div>

                        
            <div class="modal fade" id="editPackage_{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="modalTitle" aria-hidden="true">
                <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="modalTitle">Edit Package</h5>
                    </div>
                    <div class="modal-body">
                    <form action="{{route('productUpdate', ['id'=>$product->id])}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="packageName">Name</label>
                            <input type="text" class="form-control" id="packageName" name="name" required value="{{ $product->name }}">
                        </div>
                        <div class="form-group">
                            <label for="packageDesc">Description</label>
                            <input type="text" class="form-control" id="packageDesc" name="description" value="{{ $product->description }}">
                        </div>
                        <div class="form-group">
                            <label for="packagePrice">Price</label>
                            <input type="text" class="form-control" id="packagePrice" name="price" value="{{ $product->price }}">
                        </div>
                        <div class="form-group">
                            <label for="packagePricingPeriod">Pricing period</label>
                            <select class="form-control" id="packagePricingPeriod" name="price_period" value="{{ $product->price_period }}">
                            <option value="mo" selected>Monthly</option>
                            <option value="ye">Yearly</option>
                            </select>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="1" id="checkPromoted" name="promoted"  @if($product->promoted) checked="" @endif>
                            <label class="form-check-label" for="checkPromoted">
                                Promoted
                            </label>
                            </div>
                            <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="1" id="checkDiscounted" name="discounted"  @if($product->discounted) checked="" @endif>
                            <label class="form-check-label" for="checkDiscounted">
                                Discounted
                            </label>
                        </div>
                        <div class="form-footer">
                            <button type="submit" class="btn btn-primary">Save Now</button>
                        </div>
                    </form>
                    </div>
                </div>
                </div>
            </div>
            @endforeach
          
        </div>
    </div>
</div>

<div class="modal fade" id="addPackage" tabindex="-1" role="dialog" aria-labelledby="modalTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalTitle">Add Package</h5>
      </div>
      <div class="modal-body">
        <form action="productSave" method="POST">
            @csrf
            <div class="form-group">
                <label for="packageName">Name</label>
                <input type="text" class="form-control" id="packageName" name="name" required>
            </div>
            <div class="form-group">
                <label for="packageDesc">Description</label>
                <input type="text" class="form-control" id="packageDesc" name="description">
            </div>
            <div class="form-group">
                <label for="packagePrice">Price</label>
                <input type="text" class="form-control" id="packagePrice" name="price">
            </div>
            <div class="form-group">
                <label for="packagePricingPeriod">Pricing period</label>
                <select class="form-control" id="packagePricingPeriod" name="price_period">
                <option value="mo" selected>Monthly</option>
                <option value="ye">Yearly</option>
                </select>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="checkPromoted" name="promoted">
                <label class="form-check-label" for="checkPromoted">
                    Promoted
                </label>
                </div>
                <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="checkDiscounted" name="discounted">
                <label class="form-check-label" for="checkDiscounted">
                    Discounted
                </label>
            </div>
            <div class="form-footer">
                <button type="submit" class="btn btn-primary">Add Now</button>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="addFeature" tabindex="-1" role="dialog" aria-labelledby="modalTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalTitle">Add Feature</h5>
      </div>
      <div class="modal-body">
        <form action="featureSave" method="POST">
            <div class="form-group">
                @csrf
                <label for="packageName">Name</label>
                <input type="text" class="form-control" id="featureName" name="name" required>
            </div>
            <div class="form-group">
                <label for="packagePricingPeriod">Pricing period</label>
                <select class="form-control" id="packagePricingPeriod" name="product_id" required>
               @foreach ($products as $product) <div value="{{ $product->id}}">{{ $product->name }}</div>
             @endforeach
                </select>
            </div>
            <div class="form-footer">
                <button type="submit" class="btn btn-primary">Add Now</button>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
